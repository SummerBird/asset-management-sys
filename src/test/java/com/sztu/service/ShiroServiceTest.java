package com.sztu.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ShiroServiceTest {
    @Autowired
    ShiroUserService shiroUserService;
    @Test
    void testGetMap(){
        System.out.println(shiroUserService.getPwdByAccount("admin"));
    }
    @Test
    void testGetRes(){
        System.out.println(shiroUserService.getRolesAndResByAccount("admin"));
    }
}
