package com.sztu.service;

import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class DeviceServicesTest {
    @Autowired
    RoomService roomService;
    @Autowired
    DeviceService deviceService;
    @Test
    void testRoom(){
        System.out.println(roomService.getAllRoom());
    }
    @Test
    void testExportExcel() throws IOException {
        File file=new File("C:\\Users\\strip\\Desktop\\test.xls");
        file.createNewFile();
        Workbook workbook=deviceService.exportExcel();
        workbook.write(new FileOutputStream(file));
    }
    @Test
    void testImport(){
        File file=new File("C:\\Users\\strip\\Desktop\\test.xls");
        deviceService.importExcel(file);
    }
}
