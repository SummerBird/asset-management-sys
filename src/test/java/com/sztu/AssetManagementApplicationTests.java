package com.sztu;

import com.sztu.mapper.UserMapper;
import com.sztu.utils.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.sql.SQLException;


@SpringBootTest
class AssetManagementApplicationTests {
    @Test
    void contextLoads() {
    }

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    RedisUtil shiroRedisUtil;
    @Test
    void testUtils(){
        System.out.println(redisUtil.get("name"));
    }

    @Autowired
    RedisTemplate shiroRedisTemplate;
    @Test
    void testTem(){
        System.out.println(Long.parseLong("goo"));
    }
}
