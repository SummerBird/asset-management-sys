package com.sztu.token;

public enum LoginType {
        USER("com.sztu.shirocore.UserRealm"),  SHIRO_USER("com.sztu.shirocore.ShiroUserRealm");

        private String type;

        private LoginType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return this.type.toString();
        }
}
