package com.sztu.token;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;
import org.springframework.stereotype.Component;

@Data
@Accessors(chain = true)
@Component
@AllArgsConstructor
public class JwtToken implements HostAuthenticationToken, RememberMeAuthenticationToken {
    private String token;
    private char[] password;
    private boolean rememberMe;
    private String host;
    private LoginType loginType;

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return password;
    }

    public JwtToken(){
        rememberMe=false;
        loginType=LoginType.USER;
    }
    public JwtToken(String token){
        this.token=token;
    }
    public JwtToken(String token,char[] password){
        this(token,password,false,null,LoginType.USER);
    }
    public JwtToken(String token,String password){
        this(token,(char[]) (password!=null?password.toCharArray():null),false,null,LoginType.USER);
    }
    public JwtToken(String token,char[] password,String host){
        this(token,password,false,host,LoginType.USER);
    }
    public JwtToken(String token,String password,String host){
        this(token,(char[]) (password!=null?password.toCharArray():null),false,host,LoginType.USER);
    }
    public JwtToken(String token,char[] password,boolean rememberMe){
        this(token,password,rememberMe,null,LoginType.USER);
    }
    public JwtToken(String token,String password,boolean rememberMe){
        this(token,(char[]) (password!=null?password.toCharArray():null),rememberMe,null,LoginType.USER);
    }
    public JwtToken(String token,String password,boolean rememberMe,String host){
        this(token,(char[]) (password!=null?password.toCharArray():null),rememberMe,host,LoginType.USER);
    }
}
