package com.sztu.controller;

import com.sztu.service.AssetService;
import com.sztu.utils.Result;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.oas.annotations.EnableOpenApi;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@EnableOpenApi
@RestController
public class AssetController {
    private final AssetService assetService;

    public AssetController(AssetService assetService) {
        this.assetService = assetService;
    }

    @GetMapping("/getAssets")
    public Result getAssets(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("AssetList",assetService.getAllAsset());
        return new Result().setCode(200).setData(data);
    }

    @PostMapping("/delAssetByIdList")
    public Result delAssetByIdList(@RequestBody List<Integer> idList){
        if(assetService.delAssetByIdList(idList)==1){
            return new Result().setCode(200).setMessage("删除成功");
        }else {
            return new Result().setCode(202).setMessage("请刷新重试");
        }
    }

    @PostMapping("/updateAssetByMap")
    public Result updateAssetByMap(@RequestBody Map<String,Object> map){
        if(assetService.updateAsset(map)==1){
            return new Result().setCode(200).setMessage("修改成功");
        }else {
            return new Result().setCode(202).setMessage("修改失败");
        }
    }
    @PostMapping("/addAssetByMap")
    public Result addAssetByMap(@RequestBody Map<String,Object> map){
        if(assetService.addAsset(map)==1){
            return new Result().setCode(200).setMessage("添加成功");
        }else {
            return new Result().setCode(202).setMessage("添加失败");
        }
    }
    @PostMapping("/bulkEditing")
    public Result bulkEditing(@RequestParam("idList")List<Integer> idList,@RequestParam("prop")String prop,@RequestParam("val")String val){
        try {
            assetService.bulkEditAsset(idList,prop,val);
            return new Result().setCode(200).setMessage("批量修改成功");
        }catch (Exception e){
            return new Result().setCode(202).setMessage("批量修改失败,请查看类型输入类型是否正确，页面已自动刷新");
        }
    }
    @GetMapping("/downloadAllAssetExcel")
    public void downloadAllAssetExcel(HttpServletResponse response) throws IOException {
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=allAssetList.xls");
        Workbook workbook=assetService.exportExcel();
        OutputStream outputStream=response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

    @PostMapping("/uploadAssetExcel")
    public Result uploadAssetExcel(@RequestParam("file")MultipartFile[] multipartFiles){
        try {
            for (MultipartFile multipartFile : multipartFiles){
                String[] fileName=multipartFile.getOriginalFilename().split("\\.");
                File tempFile=File.createTempFile("assetExcel"+fileName[0],fileName[1]);
                multipartFile.transferTo(tempFile);
                assetService.importExcel(tempFile);
                tempFile.delete();
            }
            return new Result().setCode(200).setMessage("导入成功");
        }catch (Exception e){
            System.out.println(e);
            return new Result().setCode(202).setMessage("导入失败，请检查excel格式，建议重新下然后修改重试");
        }
    }
}
