package com.sztu.controller;

import com.sztu.service.ShiroUserService;
import com.sztu.utils.Result;
import org.apache.ibatis.annotations.Update;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ShiroUserController {
    private final ShiroUserService shiroUserService;

    private static Logger logger = Logger.getLogger(ShiroUserController.class);

    public ShiroUserController(ShiroUserService shiroUserService) {
        this.shiroUserService = shiroUserService;
    }

    @GetMapping("/getUser")
    public Result getUser(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",shiroUserService.getAllShiroUser());
        return new Result().setCode(200).setData(data);
    }

    @PostMapping("/updateUser")
    public Result updateResource(@RequestBody Map map){
        try{
            shiroUserService.updateShiroUserByMap(map);
            return new Result().setCode(200).setMessage("修改成功");
        }catch (Exception e){
            logger.error("用户修改异常"+e);
            return new Result().setCode(202).setMessage("修改失败");
        }
    }

    @PostMapping("/insertUser")
    public Result insertResource(@RequestBody Map map){
        try {
            shiroUserService.addShiroUserByMap(map);
            return new Result().setCode(200).setMessage("添加成功").setData(map);
        }catch (Exception e){
            logger.error("用户添加异常"+e);
            return new Result().setCode(202).setMessage("添加失败");
        }
    }


    @PostMapping("/deleteUsers")
    public Result deleteResources(@RequestBody List<String> list){
        try {
            shiroUserService.deleteShiroUserList(list);
            return new Result().setCode(200).setMessage("删除成功");
        }catch (Exception e){
            logger.error("用户删除异常"+e);
            return new Result().setCode(202).setMessage("删除失败");
        }
    }

    @GetMapping("/getUserPermission")
    public Result getUserPermission(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",shiroUserService.getAllPermissionOfUser());
        return new Result().setCode(200).setData(data);
    }

    @PostMapping("/updateUserPermission")
    public Result updateUserPermission(@RequestBody Map map){
        try {
            shiroUserService.updateUserPermission(map);
            return new Result().setCode(200).setMessage("更新成功").setData(map);
        }catch (Exception e){
            logger.error("用户权限修改异常"+e);
            return new Result().setCode(202).setMessage("更新失败");
        }
    }


    @GetMapping("/getRoleList")
    public Result getRoleList(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",shiroUserService.getAllRoleList());
        return new Result().setCode(200).setData(data);
    }

    @PostMapping("/setPassword")
    public Result setPassword(@RequestBody Map<String,Object> map){
        try {
            shiroUserService.setPassword(map);
            return new Result().setCode(200).setMessage("密码更新成功").setData(map);
        }catch (Exception e){
            logger.error("用户密码修改异常"+e);
            return new Result().setCode(202).setMessage("密码更新失败");
        }
    }
}
