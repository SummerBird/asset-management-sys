package com.sztu.controller;

import com.sztu.service.PermissionService;
import com.sztu.utils.Result;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PermissionController {
    private final PermissionService permissionService;

    private static Logger logger = Logger.getLogger(PermissionController.class);

    public PermissionController(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @GetMapping("/getResources")
    public Result getResources(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",permissionService.getResources());
        return new Result().setCode(200).setData(data);
    }

    @PostMapping("/updateResource")
    public Result updateResource(@RequestBody Map map){
        try{
            permissionService.updateResourceByMap(map);
            return new Result().setCode(200).setMessage("修改成功");
        }catch (Exception e){
            logger.error("权限资源修改异常"+e);
            return new Result().setCode(202).setMessage("修改失败");
        }
    }

    @PostMapping("/insertResource")
    public Result insertResource(@RequestBody Map map){
        try {
            permissionService.addResource(map);
            return new Result().setCode(200).setMessage("添加成功").setData(map);
        }catch (Exception e){
            logger.error("权限资源添加异常"+e);
            return new Result().setCode(202).setMessage("添加失败");
        }
    }

    @PostMapping("/deleteResources")
    public Result deleteResources(@RequestBody List<String> list){
        try {
            permissionService.deleteResourceByList(list);
            return new Result().setCode(200).setMessage("删除成功");
        }catch (Exception e){
            logger.error("权限资源删除异常"+e);
            return new Result().setCode(202).setMessage("删除失败");
        }
    }
}
