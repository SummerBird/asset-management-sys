package com.sztu.controller;

import com.sztu.service.StudentService;
import com.sztu.utils.Result;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.oas.annotations.EnableOpenApi;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.Map;

@EnableOpenApi
@RestController
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/getStudent")
    public Result getStudent(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("studentList",studentService.getAllStudent());
        return new Result().setCode(200).setData(data);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "name",value = "学生名字"),
            @ApiImplicitParam(name = "account",value = "学生账号"),
            @ApiImplicitParam(name = "password",value = "学生密码"),
            @ApiImplicitParam(name = "authority",value = "学生权限"),
            @ApiImplicitParam(name = "cid",value = "学生对应的学院id"),
    })
    @PostMapping("/addStudent")
    public Result addStudent(@RequestBody Map map){
        if(studentService.addStudent(map)==1){
            return new Result().setCode(200).setMessage("添加成功");
        }else {
            return new Result().setCode(202).setMessage("添加失败");
        }
    }
    @GetMapping("/delStudent/{id}")
    public Result delStudent(@PathVariable("id")Integer id){
        if(studentService.delStudentById(id)==1){
            return new Result().setCode(200).setMessage("删除成功");
        }else {
            return new Result().setCode(202).setMessage("删除失败");
        }
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "学生数据库中的id",required = true),
            @ApiImplicitParam(name = "name",value = "学生名字"),
            @ApiImplicitParam(name = "account",value = "学生账号"),
            @ApiImplicitParam(name = "password",value = "学生密码"),
            @ApiImplicitParam(name = "authority",value = "学生权限"),
            @ApiImplicitParam(name = "cid",value = "学生对应的学院id"),
    })
    @PostMapping("/updateStudent")
    public Result updateStudent(@RequestBody Map map){
        if(studentService.updateStudent(map)==1){
            return new Result().setCode(200).setMessage("更新成功");
        }else {
            return new Result().setCode(202).setMessage("更新失败");
        }
    }
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name",value = "学生名字"),
            @ApiImplicitParam(name = "account",value = "学生账号"),
            @ApiImplicitParam(name = "password",value = "学生密码"),
            @ApiImplicitParam(name = "authority",value = "学生权限"),
            @ApiImplicitParam(name = "cid",value = "学生对应的学院id"),
            @ApiImplicitParam(name = "collegeName",value = "学生对应的学院名称"),
    })
    @PostMapping("/queryStudent")
    public Result queryStudent(@RequestBody Map map){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("teacherList",studentService.queryStudent(map));
        return new Result().setCode(200).setData(data);
    }
    @GetMapping("/downloadStudentExcel")
    public void downloadStudentExcel(HttpServletResponse response) throws IOException {
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=studentList.xls");
        Workbook workbook=studentService.exportExcel();
        OutputStream outputStream=response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

    @PostMapping("/uploadStudentExcel")
    public Result uploadStudentExcel(@RequestParam("file") MultipartFile[] multipartFiles) throws IOException {
        for (MultipartFile multipartFile : multipartFiles){
            String[] fileName=multipartFile.getOriginalFilename().split("\\.");
            File tempFile=File.createTempFile(fileName[0],fileName[1]);
            multipartFile.transferTo(tempFile);
            studentService.importExcel(tempFile);
            tempFile.delete();
        }
        return new Result().setCode(200).setMessage("导入数据成功");
    }
}
