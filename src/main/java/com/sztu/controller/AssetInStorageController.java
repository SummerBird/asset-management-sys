package com.sztu.controller;

import com.sztu.service.AssetInStorageService;
import com.sztu.utils.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
public class AssetInStorageController {
    private final AssetInStorageService assetInStorageService;

    public AssetInStorageController(AssetInStorageService assetInStorageService) {
        this.assetInStorageService = assetInStorageService;
    }
    @GetMapping("/getAssetInStorageList")
    public Result getAssetInStorageList(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",assetInStorageService.getAllAssetInStorage());
        return new Result().setCode(200).setData(data);
    }
    @GetMapping("/getAssetNotInStorageList")
    public Result getAssetNotInStorageList(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("AssetNotInStorageList",assetInStorageService.getAllAssetNotInStorage());
        return new Result().setCode(200).setData(data);
    }
}
