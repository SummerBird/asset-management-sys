package com.sztu.controller;
import com.sztu.service.DeviceService;
import com.sztu.utils.Result;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class DeviceController {
    private final DeviceService deviceService;

    private static Logger logger = Logger.getLogger(DeviceController.class);

    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping("/getDeviceState")
    public Result getDeviceState(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",deviceService.getDeviceState());
        return new Result().setCode(200).setData(data);
    }


    @PostMapping("/updateDeviceState")
    public Result updateDeviceState(@RequestBody Map map){
        try{
            deviceService.updateDeviceStateByMap(map);
            return new Result().setCode(200).setMessage("修改成功");
        }catch (Exception e){
            logger.error("设备状态修改异常"+e);
            return new Result().setCode(202).setMessage("修改失败");
        }
    }

    @PostMapping("/insertDeviceState")
    public Result insertDeviceState(@RequestBody Map map){
        try {
            deviceService.addDeviceState(map);
            return new Result().setCode(200).setMessage("添加成功").setData(map);
        }catch (Exception e){
            logger.error("设备状态添加异常"+e);
            return new Result().setCode(202).setMessage("添加失败");
        }
    }


    @PostMapping("/deleteDeviceState")
    public Result deleteDeviceState(@RequestBody List<String> list){
        try {
            deviceService.deleteDeviceStateByList(list);
            return new Result().setCode(200).setMessage("删除成功");
        }catch (Exception e){
            logger.error("设备状态删除异常"+e);
            return new Result().setCode(202).setMessage("删除失败");
        }
    }

    /**
     * 设备类型相关接口
     * @return
     */


    @GetMapping("/getDeviceType")
    public Result getDeviceType(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",deviceService.getDeviceType());
        return new Result().setCode(200).setData(data);
    }


    @PostMapping("/updateDeviceType")
    public Result updateDeviceType(@RequestBody Map map){
        try{
            deviceService.updateDeviceTypeByMap(map);
            return new Result().setCode(200).setMessage("修改成功");
        }catch (Exception e){
            logger.error("设备类型修改异常"+e);
            return new Result().setCode(202).setMessage("修改失败");
        }
    }

    @PostMapping("/insertDeviceType")
    public Result insertDeviceType(@RequestBody Map map){
        try {
            deviceService.addDeviceType(map);
            return new Result().setCode(200).setMessage("添加成功").setData(map);
        }catch (Exception e){
            logger.error("设备类型添加异常"+e);
            return new Result().setCode(202).setMessage("添加失败");
        }
    }


    @PostMapping("/deleteDeviceType")
    public Result deleteDeviceType(@RequestBody List<String> list){
        try {
            deviceService.deleteDeviceTypeByList(list);
            return new Result().setCode(200).setMessage("删除成功");
        }catch (Exception e){
            logger.error("设备状态删除异常"+e);
            return new Result().setCode(202).setMessage("删除失败");
        }
    }

    @GetMapping("/getDevice")
    public Result getDevice(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",deviceService.getDevice());
        return new Result().setCode(200).setData(data);
    }

    @PostMapping("/updateDevice")
    public Result updateDevice(@RequestBody Map map){
        try{
            deviceService.updateDeviceByMap(map);
            return new Result().setCode(200).setMessage("修改成功");
        }catch (Exception e){
            logger.error("设备修改异常"+e);
            return new Result().setCode(202).setMessage("修改失败");
        }
    }

    @PostMapping("/insertDevice")
    public Result insertDevice(@RequestBody Map map){
        try {
            deviceService.addDevice(map);
            return new Result().setCode(200).setMessage("添加成功").setData(map);
        }catch (Exception e){
            logger.error("设备添加异常"+e);
            return new Result().setCode(202).setMessage("添加失败");
        }
    }

    @PostMapping("/deleteDevice")
    public Result deleteDevice(@RequestBody List<String> list){
        try {
            deviceService.deleteDeviceByList(list);
            return new Result().setCode(200).setMessage("删除成功");
        }catch (Exception e){
            logger.error("设备状态删除异常"+e);
            return new Result().setCode(202).setMessage("删除失败");
        }
    }

    @GetMapping("getDeviceInfo")
    public Result getDeviceInfo(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",deviceService.getDeviceInfo());
        return new Result().setCode(200).setData(data);
    }


    @GetMapping("/downloadDeviceExcel")
    public void downloadTeacherExcel(HttpServletResponse response) throws IOException {
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=teacherList.xls");
        Workbook workbook=deviceService.exportExcel();
        OutputStream outputStream=response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

    @PostMapping("/uploadDeviceExcel")
    public Result upload(@RequestParam("file") MultipartFile[] multipartFiles) throws IOException {
        for (MultipartFile multipartFile : multipartFiles){
            String[] fileName=multipartFile.getOriginalFilename().split("\\.");
            File tempFile=File.createTempFile(fileName[0],fileName[1]);
            multipartFile.transferTo(tempFile);
            deviceService.importExcel(tempFile);
            tempFile.delete();
        }
        return new Result().setCode(200).setMessage("导入数据成功");
    }
}
