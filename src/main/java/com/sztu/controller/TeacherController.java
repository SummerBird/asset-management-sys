package com.sztu.controller;

import com.sztu.service.TeacherService;
import com.sztu.utils.Result;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.oas.annotations.EnableOpenApi;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.Map;

@EnableOpenApi
@RestController
public class TeacherController {
    @Autowired
    TeacherService teacherService;

    @RequestMapping("/getTeachers")
    public Result getTeachers(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("teacherList",teacherService.getAllTeacher());
        return new Result().setCode(200).setData(data);
    }
    @PostMapping("/addTeacher")
    public Result addTeacher(@RequestBody Map map){
        if(teacherService.addTeacher(map)==1){
            return new Result().setCode(200).setMessage("添加成功");
        }else {
            return new Result().setCode(202).setMessage("添加失败");
        }
    }
    @GetMapping("/delTeacher/{id}")
    public Result delTeacher(@PathVariable("id")Integer id){
        if(teacherService.delTeacherById(id)==1){
            return new Result().setCode(200).setMessage("删除成功");
        }else {
            return new Result().setCode(202).setMessage("删除失败");
        }
    }
    @PostMapping("/updateTeacher")
    public Result updateTeacher(@RequestBody Map map){
        if(teacherService.updateTeacher(map)==1){
            return new Result().setCode(200).setMessage("更新成功");
        }else {
            return new Result().setCode(202).setMessage("更新失败");
        }
    }
    @PostMapping("/queryTeacher")
    public Result queryTeacher(@RequestBody Map map){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("teacherList",teacherService.queryTeacher(map));
        return new Result().setCode(200).setData(data);
    }
    @GetMapping("/downloadTeacherExcel")
    public void downloadTeacherExcel(HttpServletResponse response) throws IOException {
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=teacherList.xls");
        Workbook workbook=teacherService.exportExcel();
        OutputStream outputStream=response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }
    @PostMapping("/uploadTeacherExcel")
    public Result upload(@RequestParam("file") MultipartFile[] multipartFiles) throws IOException {
        for (MultipartFile multipartFile : multipartFiles){
            String[] fileName=multipartFile.getOriginalFilename().split("\\.");
            File tempFile=File.createTempFile(fileName[0],fileName[1]);
            multipartFile.transferTo(tempFile);
            teacherService.importExcel(tempFile);
            tempFile.delete();
        }
        return new Result().setCode(200).setMessage("导入数据成功");
    }
}
