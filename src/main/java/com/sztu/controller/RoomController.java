package com.sztu.controller;

import com.sztu.service.RoomService;
import com.sztu.service.ShiroUserService;
import com.sztu.utils.Result;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RoomController {
    private final RoomService roomService;

    private final ShiroUserService shiroUserService;

    private static Logger logger = Logger.getLogger(RoomController.class);

    public RoomController(RoomService roomService, ShiroUserService shiroUserService) {
        this.roomService = roomService;
        this.shiroUserService = shiroUserService;
    }

    @GetMapping("/getRoom")
    public Result getRoom(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",roomService.getAllRoom());
        return new Result().setCode(200).setData(data);
    }


    @PostMapping("/updateRoom")
    public Result updateResource(@RequestBody Map map){

        try{
            roomService.updateRoomByMap(map);
            return new Result().setCode(200).setMessage("修改成功");
        }catch (Exception e){
            logger.error("房间修改异常"+e);
            return new Result().setCode(202).setMessage("修改失败");
        }
    }

    @PostMapping("/insertRoom")
    public Result insertResource(@RequestBody Map map){
        try {
            roomService.addRoomByMap(map);
            return new Result().setCode(200).setMessage("添加成功").setData(map);
        }catch (Exception e){
            logger.error("房间添加异常"+e);
            return new Result().setCode(202).setMessage("添加失败");
        }
    }


    @PostMapping("/deleteRooms")
    public Result deleteResources(@RequestBody List<String> list){
        try {
            roomService.deleteRoomByList(list);
            return new Result().setCode(200).setMessage("删除成功");
        }catch (Exception e){
            logger.error("房间删除异常"+e);
            return new Result().setCode(202).setMessage("删除失败");
        }
    }

    @GetMapping("/getPrincipalUsers")
    public Result getPrincipalUsers(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",shiroUserService.getPrincipalUsers());
        return new Result().setCode(200).setData(data);
    }
}
