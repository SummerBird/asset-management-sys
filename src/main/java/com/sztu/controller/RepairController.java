package com.sztu.controller;

import com.sztu.service.RepairService;
import com.sztu.utils.Result;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
public class RepairController {
    private final RepairService repairService;

    private static Logger logger = Logger.getLogger(RepairController.class);

    public RepairController(RepairService repairService) {
        this.repairService = repairService;
    }

    @PostMapping("/repair")
    public Result repair(@RequestBody Map<String,Object> deviceInfo){
        try {
            repairService.repair(deviceInfo);
            return new Result().setCode(200).setMessage("维修申请成功，请等待申请通过");
        }catch (Exception e){
            logger.error(e);
            return new Result().setCode(202).setMessage("维修申请失败");
        }
    }

    @GetMapping("/getRepairList/{userId}")
    public Result getRepairList(@PathVariable("userId")Long userId){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",repairService.getNeedsRepair(userId));
        return new Result().setCode(200).setData(data);
    }

    @GetMapping("/getReportList/{userId}")
    public Result getReportList(@PathVariable("userId")Long userId){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("tableData",repairService.getRepairing(userId));
        return new Result().setCode(200).setData(data);
    }


    @PostMapping("/changeRepairText")
    public Result changeRepairText(@RequestBody Map<String,Object> repairInfo){
        try {
            repairService.approveRepair(repairInfo);
            return new Result().setCode(200).setMessage("修改维修状态成功");
        }catch (Exception e){
            logger.error(e);
            return new Result().setCode(202).setMessage("修改维修状态失败");
        }
    }

}
