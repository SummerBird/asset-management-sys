package com.sztu.controller;

import com.sztu.pojo.College;
import com.sztu.service.CollegeService;
import com.sztu.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.oas.annotations.EnableOpenApi;
import java.util.LinkedHashMap;
import java.util.Map;

@EnableOpenApi
@RestController
public class CollegeController {
    @Autowired
    CollegeService collegeService;

    @RequestMapping("/getColleges")
    public Result getColleges(){
        Map<String,Object> data=new LinkedHashMap<String,Object>();
        data.put("collegeList",collegeService.getAllCollege());
        return new Result().setCode(200).setData(data);
    }
    @PostMapping("/updateCollege")
    public Result updateCollege(@RequestBody College college){
        if(collegeService.updateCollege(college)==1){
            return new Result().setCode(200).setMessage("修改成功");
        }else {
            return new Result().setCode(202).setMessage("修改失败");
        }
    }
    @PostMapping("/addCollege")
    public Result addCollege(@RequestBody College college){
        if(collegeService.addCollege(college)==1){
            return new Result().setCode(200).setMessage("添加成功");
        }else{
            return new Result().setCode(202).setMessage("添加失败");
        }
    }
    @GetMapping("/delCollege/{id}")
    public Result delCollege(@PathVariable("id") Long id){

        if(collegeService.delCollegeById(id)==1){
            return new Result().setCode(200).setMessage("删除成功");
        }else{
            return new Result().setCode(202).setMessage("删除失败");
        }
    }
}
