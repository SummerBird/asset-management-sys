package com.sztu.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;


import java.sql.Timestamp;
import java.util.Date;

@Slf4j
@Component
public class AutoMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill....");
        this.setFieldValByName("gmtCreate", new Timestamp(new Date().getTime()),metaObject);
        this.setFieldValByName("gmtModified", new Timestamp(new Date().getTime()),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill....");
        this.setFieldValByName("gmtModified", new Timestamp(new Date().getTime()),metaObject);
    }
}
