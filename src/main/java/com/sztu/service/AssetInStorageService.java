package com.sztu.service;

import com.sztu.pojo.Asset;
import com.sztu.pojo.AssetInStorage;

import java.util.List;
import java.util.Map;

public interface AssetInStorageService {
    public abstract List<AssetInStorage> getAllAssetInStorage();
    public abstract List<Asset> getAllAssetNotInStorage();
    public abstract void addAssetInStorage(Map map);
    public abstract void delAssetInStorageById(Integer id);
}
