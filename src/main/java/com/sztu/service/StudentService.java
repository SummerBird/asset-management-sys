package com.sztu.service;

import com.sztu.pojo.Student;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface StudentService {
    public List<Student> getAllStudent();
    public List<Student> queryStudent(Map map);
    public int addStudent(Map map);
    public int delStudentById(Integer id);
    public int updateStudent(Map map);
    public abstract Workbook exportExcel();
    public abstract Integer importExcel(File file);
}
