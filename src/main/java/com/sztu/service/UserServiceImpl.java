package com.sztu.service;

import com.sztu.mapper.UserMapper;
import com.sztu.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> getUsers() {
        return userMapper.getUsers();
    }

    @Override
    public User queryUserByNameAndPwd(String name, String pwd) {
        return userMapper.queryUserByNameAndPwd(name,pwd);
    }

    @Override
    public User queryUserByName(String name) {
        return userMapper.queryUserByName(name);
    }
}
