package com.sztu.service;

import java.util.List;
import java.util.Map;

public interface RepairService {
    public void repair(Map<String,Object> repairInfo) throws Exception;

    public void approveRepair(Map<String,Object> approveInfo);

    public void changeRepairInfo(Map<String,Object> repairInfo);

    public List<Map<String,Object>> getNeedsRepair(Long UserId);

    public List<Map<String,Object>> getRepairing(Long UserId);
}
