package com.sztu.service;

import com.sztu.pojo.User;
import java.util.List;

public interface UserService {
    public abstract List<User> getUsers();
    public abstract User queryUserByNameAndPwd(String name,String pwd);
    public abstract User queryUserByName(String name);
}
