package com.sztu.service;

import com.sztu.pojo.DeviceState;
import com.sztu.pojo.DeviceType;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface DeviceService {
    public List<DeviceType> getDeviceType();
    public void updateDeviceTypeByMap(Map<String,Object> map);
    public void addDeviceType(Map<String,Object> map);
    public void deleteDeviceTypeByList(List<String> idList);


    public List<Map<String,Object>> getDevice();
    public void updateDeviceByMap(Map<String,Object> map);
    public void addDevice(Map<String,Object> map);
    public void deleteDeviceByList(List<String> idList);

    public List<DeviceState> getDeviceState();
    public void updateDeviceStateByMap(Map<String,Object> map);
    public void addDeviceState(Map<String,Object> map);
    public void deleteDeviceStateByList(List<String> idList);

    public List<Map<String,Object>> getDeviceInfo();
    public Boolean canBeBorrowed(Long deviceId);
    public void setDeviceState(Long deviceId,Long stateId);

    public Integer importExcel(File file);
    public Workbook exportExcel();
}
