package com.sztu.service;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.sztu.mapper.AssetMapper;
import com.sztu.pojo.Asset;
import com.sztu.utils.StringUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AssetServiceImpl implements AssetService {
    private final AssetMapper assetMapper;
    public AssetServiceImpl(AssetMapper assetMapper) {
        this.assetMapper = assetMapper;
    }

    @Override
    public List<Asset> getAllAsset() {
        return assetMapper.getAllAsset();
    }

    @Override
    public List<Asset> queryByLike(Map map) {
        return assetMapper.queryByLike(map);
    }

    @Override
    public int addAsset(Map map) {
        Object price=map.getOrDefault("price","空");
        Object value=map.getOrDefault("value","空");
        if(!StringUtil.isNumber(price.toString())){
            map.put("price",0);
        }
        if(!StringUtil.isNumber(value.toString())){
            map.put("value",0);
        }
        return assetMapper.addAsset(map);
    }

    @Override
    public int updateAsset(Map map) {
        Object price=map.getOrDefault("price","空");
        Object value=map.getOrDefault("value","空");
        if(!StringUtil.isNumber(price.toString())||!StringUtil.isNumber(value.toString())){
            return 0;
        }
        return assetMapper.updateAsset(map);
    }

    @Override
    public int delAssetById(int id) {
        return assetMapper.delAssetById(id);
    }

    @Override
    public int delAssetByIdList(List<Integer> idList) {
        for(Integer id: idList){
            if(assetMapper.delAssetById(id)==0){
                return 0;
            }
        }
        return 1;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bulkEditAsset(List<Integer> idList, String prop, String val) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(prop, val);
        for (Integer id : idList) {
            map.put("id", id);
            assetMapper.updateAsset(map);
        }
    }

    @Override
    public Workbook exportExcel() {
        List assetList=assetMapper.getAllAsset();
        Workbook workbook= ExcelExportUtil.exportExcel(new ExportParams("资产基本信息","资产基本信息"),Asset.class,assetList);
        return workbook;
    }


    @Override
    @Transactional
    public void importExcel(File file) {
        ImportParams params=new ImportParams();
        params.setTitleRows(1);
        params.setHeadRows(1);
        List<Asset> assetList= ExcelImportUtil.importExcel(file,Asset.class,params);
        for(Asset asset:assetList){
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("id",asset.getId());
            map.put("name",asset.getName());
            map.put("serialNumber",asset.getSerialNumber());
            map.put("type",asset.getType());
            map.put("category",asset.getCategory());
            map.put("price",asset.getPrice());
            map.put("value",asset.getValue());
            map.put("manufacturers",asset.getManufacturers());
            map.put("supplier",asset.getSupplier());
            if(assetMapper.updateAsset(map)!=1)assetMapper.addAsset(map);
        }
    }

}
