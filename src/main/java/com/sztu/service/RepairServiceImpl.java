package com.sztu.service;

import com.sztu.mapper.RepairMapper;
import com.sztu.pojo.Repair;
import com.sztu.utils.LongUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RepairServiceImpl implements RepairService{

    private final RepairMapper repairMapper;

    private final DeviceService deviceService;

    public RepairServiceImpl(RepairMapper repairMapper, DeviceService deviceService) {
        this.repairMapper = repairMapper;
        this.deviceService = deviceService;
    }

    @Override
    public void repair(Map<String, Object> repairInfo) throws Exception {
        Repair repair=new Repair()
                .setDeviceId(LongUtil.parseLong(repairInfo.getOrDefault("device_id",0L)))
                .setRepairUserId(1L)
                .setReportUserId(LongUtil.parseLong(repairInfo.getOrDefault("user_id",0L)))
                .setRepairText("未确认状态")
                ;
        repairMapper.insert(repair);

    }

    @Override
    public void approveRepair(Map<String, Object> repairInfo) {
        String repairText=(String) repairInfo.getOrDefault("repair_text","维修中");
        Repair repair=new Repair()
                .setId(LongUtil.parseLong(repairInfo.getOrDefault("repair_id",null)))
                .setRepairText(repairText)
                ;

        repairMapper.updateById(repair);
        Long deviceId=LongUtil.parseLong(repairInfo.getOrDefault("device_id",null));
        if(repairText.equals("维修中")) deviceService.setDeviceState(deviceId,6L);
        else deviceService.setDeviceState(deviceId,4L);
    }

    @Override
    public void changeRepairInfo(Map<String, Object> repairInfo) {

    }

    @Override
    public List<Map<String, Object>> getNeedsRepair(Long repairUserId) {
        return repairMapper.getRepairInfoByRepairUserId(repairUserId);
    }

    @Override
    public List<Map<String, Object>> getRepairing(Long reportUserId) {
        return repairMapper.getRepairInfoByReportUserId(reportUserId);
    }
}
