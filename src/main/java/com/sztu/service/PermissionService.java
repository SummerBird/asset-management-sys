package com.sztu.service;

import com.sztu.pojo.ShiroResource;

import java.util.List;
import java.util.Map;

public interface PermissionService {
    public List<ShiroResource> getResources();
    public void updateResourceByMap(Map map);
    public void addResource(Map map);
    public void deleteResourceByList(List<String> idList);
}
