package com.sztu.service;

import java.util.List;
import java.util.Map;

public interface BorrowService {
    public void borrow(Map<String,Object> borrowInfo) throws Exception;

    public void approve(Map<String,Object> approveInfo);

    public void returnDevice(Map<String,Object> returnInfo);

    public List<Map<String,Object>> getNeedsApprove(Long UserId);

    public List<Map<String,Object>> getApprovals(Long UserId);
}
