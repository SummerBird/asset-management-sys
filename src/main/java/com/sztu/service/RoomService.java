package com.sztu.service;



import java.util.List;
import java.util.Map;

public interface RoomService {
    public List<Map<String,Object>> getAllRoom();
    public void addRoomByMap(Map<String,Object> map);
    public void updateRoomByMap(Map map);
    public void deleteRoomByList(List<String> idList);
    public Map<String,Object> getRoomInfoById(Long id);
}
