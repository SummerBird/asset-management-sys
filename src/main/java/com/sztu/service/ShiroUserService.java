package com.sztu.service;

import com.sztu.pojo.ShiroUser;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface ShiroUserService {
    public abstract ShiroUser getPwdByAccount(String account);
    public abstract List<String> getRolesById(Long id);
    public abstract List<String> getResSourceByRoles(List<String> roles);
    public abstract Map<String,List> getRolesAndResByAccount(String account);
    public abstract String getShiroNameById(Long id);
    public abstract List<Map<String,Object>> getPrincipalUsers();

    public List<Map<String,Object>> getAllShiroUser();
    public void addShiroUserByMap(Map<String,Object> map);
    public void updateShiroUserByMap(Map<String,Object> map);
    public void deleteShiroUserList(List<String> idList);


    // 用户权限相关的服务
    public List<Map<String,Object>> getAllPermissionOfUser();
    public void deleteUserPermission(Map<String,Object> map);
    public void updateUserPermission(Map<String,Object>map);
    public void addDefaultPermission(Long id);
    public List<Map<String,Object>> getAllRoleList();

    public void setPassword(Map<String,Object> userInfo);
}
