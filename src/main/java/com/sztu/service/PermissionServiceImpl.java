package com.sztu.service;

import com.sztu.mapper.ShiroResourceMapper;
import com.sztu.pojo.ShiroResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PermissionServiceImpl implements PermissionService{
    private final ShiroResourceMapper shiroResourceMapper;

    public PermissionServiceImpl(ShiroResourceMapper shiroResourceMapper) {
        this.shiroResourceMapper = shiroResourceMapper;
    }

    @Override
    public List<ShiroResource> getResources() {
        return shiroResourceMapper.selectList(null);
    }

    @Override
    public void updateResourceByMap(Map map) {
       shiroResourceMapper.updateById(
               new ShiroResource().setId(Long.parseLong((String) map.getOrDefault("id",null)))
                                  .setName((String) map.getOrDefault("name",null))
                                  .setDescription((String) map.getOrDefault("description",null))
                                  .setShiroLabel((String) map.getOrDefault("shiroLabel",null))
       );
    }

    /**
     *
     * @param map
     * @return 会在map中注入对应的数据
     */
    @Override
    public void addResource(Map map) {
        shiroResourceMapper.insertByMap(map);
    }

    /**
     *
     * @param idList 参数需要为string类型的
     */
    @Override
    public void deleteResourceByList(List<String> idList) {
        List<Long> longIdList=idList.stream().map(s -> {
            return Long.parseLong(s);
        }).collect(Collectors.toList());
        shiroResourceMapper.deleteBatchIds(longIdList);
    }
}
