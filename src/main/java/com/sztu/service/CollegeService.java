package com.sztu.service;

import com.sztu.pojo.College;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CollegeService {
    public abstract List<College> getAllCollege();
    public abstract College getCollegeById(Long id);
    public abstract College getCollegeByName(String collegeName);
    public abstract int addCollege(College college);
    public abstract int updateCollege(College college);
    public abstract int delCollegeById(Long id);
}
