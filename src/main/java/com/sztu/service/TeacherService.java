package com.sztu.service;

import com.sztu.pojo.Teacher;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface TeacherService {
    public abstract List<Teacher> getAllTeacher();
    public abstract List<Teacher> queryTeacher(Map map);
    public abstract int addTeacher(Map map);
    public abstract int delTeacherById(Integer id);
    public abstract int updateTeacher(Map map);
    public abstract Workbook exportExcel();
    public abstract Integer importExcel(File file);
}
