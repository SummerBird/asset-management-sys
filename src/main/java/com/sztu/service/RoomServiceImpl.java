package com.sztu.service;

import com.sztu.mapper.RoomMapper;
import com.sztu.pojo.Room;
import com.sztu.utils.LongUtil;
import com.sztu.utils.RedisUtil;
import com.sztu.utils.StringUtil;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RoomServiceImpl implements RoomService{

    private final RoomMapper roomMapper;

    private final RedisUtil redisUtil;

    private final ShiroUserService shiroUserService;

    public RoomServiceImpl(RoomMapper roomMapper, RedisUtil redisUtil, ShiroUserService shiroUserService) {
        this.roomMapper = roomMapper;
        this.redisUtil = redisUtil;
        this.shiroUserService = shiroUserService;
    }


    @Override
    public List<Map<String,Object>> getAllRoom() {
        List<Map<String,Object>> rooms=roomMapper.selectMaps(null);
        rooms.forEach(room->{
            String key="room:principal:name:"+room.get("principal_id");
            String principalNameCache= (String) redisUtil.get(key);
            if(principalNameCache==null){
                // 如果没有缓存，那就到数据库中查询
                principalNameCache=shiroUserService.getShiroNameById((Long) room.get("principal_id"));
                // 将其放到缓存中去
                redisUtil.set(key,principalNameCache);
                redisUtil.expire(key,60);
            }
            room.put("principal_name",principalNameCache);

        });
        return rooms;
    }

    @Override
    public void addRoomByMap(Map<String, Object> map) {
        if(!StringUtil.isNumber(map.getOrDefault("principal_id",null).toString())){
            map.put("principalId",1L);
        }
        roomMapper.insertByMap(map);
    }

    @Override
    public void updateRoomByMap(Map map) {
        Room room=generateRoomByMap(map);
        room.setId(Long.parseLong( map.get("id").toString()));
        roomMapper.updateById(room);
    }

    @Override
    public void deleteRoomByList(List<String> idList) {
        List<Long> longIdList=idList.stream().map(s -> {
            return Long.parseLong(s);
        }).collect(Collectors.toList());
        roomMapper.deleteBatchIds(longIdList);
    }

    @Override
    public Map<String, Object> getRoomInfoById(Long id) {
        Room room=roomMapper.selectById(id);
        Map<String,Object> res=new HashMap<String,Object>();
        res.put("room_no",room.getRoomNo());
        res.put("room_name",room.getRoomName());
        return res;
    }

    private Room generateRoomByMap(Map<String,Object> map){
        Room room = new Room();
        //对college_id和principal_id分别做出判断传参是否为空
        String college_id = map.getOrDefault("college_id", null).toString();
        if(college_id != null && StringUtil.isNumber(college_id)) {
            //当选项是“请输入名字”的时候，传入的college_id是“8”，默认设置为空。
            if(college_id.equals("8")){
                room.setCollegeId(null);
            }
            else room.setCollegeId(Long.valueOf(college_id));
        }else {
            room.setCollegeId(null);
        }
        String principal_id = map.getOrDefault("principal_id", null).toString();
        if(principal_id != null && StringUtil.isNumber(principal_id)) {
            room.setPrincipalId(Long.valueOf(principal_id));
        }else {
            room.setPrincipalId(null);
        }
        room .setRoomNo((String)map.getOrDefault("room_no",null));
        room.setRoomName((String)map.getOrDefault("room_name",null));
        room.setDescription((String) map.getOrDefault("description",null));
        return room;
    }
}
