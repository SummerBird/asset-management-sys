package com.sztu.service;

import com.sztu.mapper.AssetInStorageMapper;
import com.sztu.pojo.Asset;
import com.sztu.pojo.AssetInStorage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class AssetInStorageServiceImpl implements AssetInStorageService{

    private final AssetInStorageMapper assetInStorageMapper;

    public AssetInStorageServiceImpl(AssetInStorageMapper assetInStorageMapper) {
        this.assetInStorageMapper = assetInStorageMapper;
    }

    @Override
    public List<AssetInStorage> getAllAssetInStorage() {
        return assetInStorageMapper.getAllAssetInStorage();
    }

    @Override
    public List<Asset> getAllAssetNotInStorage() {
        return assetInStorageMapper.getAllAssetNotInStorage();
    }

    @Override
    @Transactional
    public void addAssetInStorage(Map map) {
        if(assetInStorageMapper.addAssetInStorage(map)!=1){
            throw new RuntimeException("添加失败");
        }
    }

    @Override
    @Transactional
    public void delAssetInStorageById(Integer id) {
        if(assetInStorageMapper.delAssetInStorageById(id)!=1){
            throw new RuntimeException("删除失败");
        }
    }
}
