package com.sztu.service;

import com.sztu.mapper.CollegeMapper;
import com.sztu.pojo.College;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollegeServiceImpl implements CollegeService{
    @Autowired
    CollegeMapper collegeMapper;
    @Override
    public List<College> getAllCollege() {
        return collegeMapper.getAllCollege();
    }

    @Override
    public College getCollegeById(Long id) {
        return collegeMapper.getCollegeById(id);
    }

    @Override
    public College getCollegeByName(String collegeName) {
        return collegeMapper.getCollegeByName(collegeName);
    }

    @Override
    public int addCollege(College college) {
        return collegeMapper.addCollege(college);
    }

    @Override
    public int updateCollege(College college) {
        return collegeMapper.updateCollege(college);
    }

    @Override
    public int delCollegeById(Long id) {
        return collegeMapper.delCollegeById(id);
    }
}
