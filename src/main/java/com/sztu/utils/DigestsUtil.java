package com.sztu.utils;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

import java.util.HashMap;
import java.util.Map;

public class DigestsUtil {
    public static final String SHA1="SHA-1";

    public static final Integer HASH_ITERATIONS=512;

    public static String sha1(String input,String salt){
        return new SimpleHash(SHA1,input,salt,HASH_ITERATIONS).toString();
    }

    public static String generateSalt(){
        SecureRandomNumberGenerator randomNumberGenerator=new SecureRandomNumberGenerator();
        return randomNumberGenerator.nextBytes().toHex();
    }
    public static Map<String,String> entryPassword(String password){
        Map<String,String> res=new HashMap<>();
        String salt=generateSalt();
        String entryPwd=sha1(password,salt);
        res.put("salt",salt);
        res.put("password",entryPwd);
        return res;
    }
}
