package com.sztu.utils;

import java.io.File;

public class CleanLogsUtil {
    public static boolean deleteAnyone(String FileName){

        File file = new File(FileName);//根据指定的文件名创建File对象
        if ( !file.exists() ){  //要删除的文件不存在
            return false;
        }else{ //要删除的文件存在

            if ( file.isFile() ){ //如果目标文件是文件

                return deleteFile(FileName);

            }else{  //如果目标文件是目录
                return deleteDir(FileName);
            }
        }
    }

    public static boolean deleteFile(String fileName){


        File file = new File(fileName);//根据指定的文件名创建File对象

        if (  file.exists() && file.isFile() ){ //要删除的文件存在且是文件

            if ( file.delete()){
                return true;
            }else{
                return false;
            }
        }else{

            return false;
        }


    }


    public static boolean deleteDir(String dirName){

        if ( dirName.endsWith(File.separator) )//dirName不以分隔符结尾则自动添加分隔符
            dirName = dirName + File.separator;

        File file = new File(dirName);//根据指定的文件名创建File对象

        if ( !file.exists() || ( !file.isDirectory() ) ){ //目录不存在或者
            System.out.println("目录删除失败"+dirName+"目录不存在！" );
            return false;
        }

        File[] fileArrays = file.listFiles();//列出源文件下所有文件，包括子目录


        for ( int i = 0 ; i < fileArrays.length ; i++ ){//将源文件下的所有文件逐个删除

            CleanLogsUtil.deleteAnyone(fileArrays[i].getAbsolutePath());

        }


        if ( file.delete() )//删除当前目录
            System.out.println("目录"+dirName+"删除成功！" );


        return true;

    }


    /**删除指定目录下的文件(包括目录) **/
    public static void main(String[] args){
        //创建目标文件路径
        String fileName = "logs";
        CleanLogsUtil.deleteAnyone(fileName);
    }
}
