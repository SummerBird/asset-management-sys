package com.sztu.utils;

public class LongUtil {
    public static Long parseLong(Object val){
        if(val==null)return 1L;
        Long v = null;
        try {
            v = Long.parseLong((String) val);
        }catch (Exception e){
            v = Long.parseLong(val.toString());
        }finally {
            if(v == null) v=new Long(1L);
        }
        return v;
    }
}
