package com.sztu.shirocore;

import com.sztu.token.JwtToken;
import com.sztu.utils.DigestsUtil;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.util.SimpleByteSource;

public class HashCredentialsMatcher extends SimpleCredentialsMatcher {
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        JwtToken jwtToken=(JwtToken) token;
        if(jwtToken.getPassword()==null) return true;
        String inputPwdPlan=new String(jwtToken.getPassword());
        SimpleAuthenticationInfo simpleAuthenticationInfo=(SimpleAuthenticationInfo) info;
        String dbPwd= (String) simpleAuthenticationInfo.getCredentials();

        // 从info获取混淆值
        SimpleByteSource saltByteSource= (SimpleByteSource) simpleAuthenticationInfo.getCredentialsSalt();
        String salt=new String(saltByteSource.getBytes());

        String inputPwd=DigestsUtil.sha1(inputPwdPlan,salt);

        return inputPwd.equals(dbPwd);
    }
}
