package com.sztu.shirocore;

import com.sztu.token.JwtToken;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

import org.springframework.stereotype.Component;

//自定义密码验证
@Component
public class JwtCredentialsMatcher extends SimpleCredentialsMatcher {
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        JwtToken jwtToken=(JwtToken) token;
        if(jwtToken.getPassword()==null) return true;
        String inPwd=new String(jwtToken.getPassword());
        String dbPwd=(String) info.getCredentials();
        return this.equals(inPwd,dbPwd);
    }
}
