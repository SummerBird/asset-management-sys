package com.sztu.config;

import com.sztu.shirocore.*;
import org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class ShiroConfig {

    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("securityManager") SecurityManager defaultWebSecurityManager){
        ShiroFilterFactoryBean bean=new ShiroFilterFactoryBean();
        bean.setSecurityManager(defaultWebSecurityManager);
        Map<String, Filter> map=new LinkedHashMap<>();
        map.put("jwt",new JwtFilter());
        Map<String,String> filterMap=new LinkedHashMap<>();
        filterMap.put("/swagger-ui/*","anon");
        filterMap.put("/druid","anon");
        filterMap.put("/druid/**","anon");
        filterMap.put("/v3/api-docs","anon");
        filterMap.put("/swagger-resources/**","anon");
        filterMap.put("/login","anon");
        filterMap.put("/userLogin","anon");
        filterMap.put("/**","jwt");
        bean.setFilters(map);
        bean.setFilterChainDefinitionMap(filterMap);
        return bean;
    }


    @Bean(name = "securityManager")
    public SecurityManager getDefaultWebSecurityManager(@Qualifier("userModularRealmAuthenticator") UserModularRealmAuthenticator userModularRealmAuthenticator, @Qualifier("userRealm") UserRealm userRealm, @Qualifier("shiroUserRealm") ShiroUserRealm shiroUserRealm){
        DefaultWebSecurityManager securityManager=new DefaultWebSecurityManager();
        // 配置自定义realm管理
        securityManager.setAuthenticator(userModularRealmAuthenticator);

        List<Realm> realms=new ArrayList<>();
        realms.add(userRealm);
        realms.add(shiroUserRealm);
        // 配置多realm
        securityManager.setRealms(realms);
        return securityManager;
    }

    @Bean
    public UserModularRealmAuthenticator userModularRealmAuthenticator(){
        UserModularRealmAuthenticator modularRealmAuthenticator=new UserModularRealmAuthenticator();
        // 允许尝试默认的策略
        modularRealmAuthenticator.setAuthenticationStrategy(new AtLeastOneSuccessfulStrategy());
        return modularRealmAuthenticator;
    }


    @Bean
    public UserRealm userRealm(){
        UserRealm userRealm=new UserRealm();
        userRealm.setCredentialsMatcher(new JwtCredentialsMatcher());
        return userRealm;
    }

    @Bean
    public ShiroUserRealm shiroUserRealm(){
        return new ShiroUserRealm();
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(@Qualifier("securityManager") SecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }
}
