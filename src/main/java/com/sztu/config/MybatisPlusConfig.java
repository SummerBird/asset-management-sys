package com.sztu.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@MapperScan("com.sztu.mapper")
public class MybatisPlusConfig {
}
