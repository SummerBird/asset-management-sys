package com.sztu.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "spring.shiro-redis")
public class ShiroRedisProperties {
    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 6379;
    private String host = DEFAULT_HOST;
    private int port = DEFAULT_PORT;
    private int database;
    private String password;
}
