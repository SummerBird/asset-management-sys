package com.sztu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@EnableOpenApi

public class SwaggerConfig implements WebMvcConfigurer {

    @Bean
    public Docket defaultApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                // 选择那些路径和api会生成document
                .apis(RequestHandlerSelectors.basePackage("com.sztu.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("资产管理系统接口文档")
                //描述
                .description("接口文档")
                .version("1.0")
                .build();

    }
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController( "/swagger/")
                .setViewName("forward:" +  "/swagger-ui/index.html");
    }
}
