package com.sztu.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.sztu.utils.RedisUtil;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;


@Configuration
@EnableConfigurationProperties({ShiroRedisProperties.class})
public class RedisConfig{

    private final ShiroRedisProperties shiroRedisProperties;
    private LettuceConnectionFactory shiroConnectionFactory;

    public RedisConfig(ShiroRedisProperties shiroRedisProperties) {
        this.shiroRedisProperties = shiroRedisProperties;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        return getSerializeRedisTemplate(redisConnectionFactory);
    }


    @Bean("shiroRedisTemplate")
    public RedisTemplate<String, Object> shiroRedisTemplate(GenericObjectPoolConfig config, RedisStandaloneConfiguration shiroRedisConfig) {

        if(shiroConnectionFactory==null){
            LettuceClientConfiguration clientConfiguration = LettucePoolingClientConfiguration.builder().poolConfig(config).build();
            shiroConnectionFactory=new LettuceConnectionFactory(shiroRedisConfig, clientConfiguration);
            shiroConnectionFactory.afterPropertiesSet();
        }
        return getSerializeRedisTemplate(shiroConnectionFactory);
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.redis.lettuce.pool")
    public GenericObjectPoolConfig redisPool() {
        return new GenericObjectPoolConfig<>();
    }


    @Bean("shiroRedisConfig")
    public RedisStandaloneConfiguration shiroRedisConfig() {
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
        config.setHostName(shiroRedisProperties.getHost());
        config.setPort(shiroRedisProperties.getPort());
        config.setPassword(RedisPassword.of(shiroRedisProperties.getPassword()));
        config.setDatabase(shiroRedisProperties.getDatabase());
        return config;
    }




    @Bean("redisUtil")
    public RedisUtil redisUtil(RedisTemplate<String,Object> redisTemplate){
        return new RedisUtil(redisTemplate);
    }

    @Bean("shiroRedisUtil")
    public RedisUtil shiroRedisUtil(RedisTemplate<String,Object> shiroRedisTemplate){
        return new RedisUtil(shiroRedisTemplate);
    }


    public RedisTemplate<String, Object> getSerializeRedisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate<String, Object> template = new RedisTemplate();
        template.setConnectionFactory(redisConnectionFactory);
        // 序列化配置
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer=new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om=new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        template.setKeySerializer(stringRedisSerializer);
        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }



}
