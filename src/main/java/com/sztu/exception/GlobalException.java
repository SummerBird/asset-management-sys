package com.sztu.exception;

import com.sztu.utils.Result;
import org.apache.log4j.Logger;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalException {
    private static Logger logger = Logger.getLogger(GlobalException.class);
    @ExceptionHandler(value = UnauthorizedException.class)
    public Result handler(UnauthorizedException e){
        logger.error("运行时异常-----"+e.getMessage());
        return new Result().setCode(401).setMessage("无权限访问");
    }
    @ExceptionHandler(value = ExpiredCredentialsException.class)
    public Result handler(ExpiredCredentialsException e){
        logger.error("运行时异常-----"+e.getMessage());
        return new Result().setCode(401).setMessage("登录已过期，请重新登录");
    }
    @ExceptionHandler(value = UnauthenticatedException.class)
    public Result handler(UnauthenticatedException e){
        logger.error("运行时异常-----"+e.getMessage());
        return new Result().setCode(401).setMessage("未登录");
    }
}
