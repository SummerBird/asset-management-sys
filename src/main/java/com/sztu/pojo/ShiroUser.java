package com.sztu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Accessors(chain = true)
public class ShiroUser implements Serializable{
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String name;

    private String account;

    private String password;

    private String salt;

    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;

    private Long collegeId;
}
