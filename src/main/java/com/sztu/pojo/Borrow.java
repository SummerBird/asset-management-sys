package com.sztu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zjx
 * @since 2021-12-13
 */
@Data
@Accessors(chain = true)
public class Borrow implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long deviceId;

    private Long userId;

    private Long approveUserId;

    private Boolean approveResult;

    private String approveText;

    private Boolean isReturn;

    private Timestamp useDate;

    private String description;

    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;


}
