package com.sztu.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zjx
 * @since 2021-12-21
 */
@Data
@Accessors(chain = true)
public class Repair implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long deviceId;

    private Long repairNo;

    private String repairText;

    private Long reportUserId;

    private Long repairUserId;

    private String state;

    private Timestamp repairDate;

    private String description;

    @TableField(fill = FieldFill.INSERT)
    private Timestamp gmtCreate;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Timestamp gmtModified;


}
