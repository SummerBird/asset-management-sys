package com.sztu.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class College {
    @Excel(name = "学院id")
    Long id;
    @Excel(name = "学院")
    String collegeName;
}
