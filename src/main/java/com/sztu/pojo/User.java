package com.sztu.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class User {
    Integer id;
    @JsonProperty("username")
    String name;
    @JsonProperty("password")
    String pwd;
}
