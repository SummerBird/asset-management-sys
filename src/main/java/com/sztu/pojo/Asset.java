package com.sztu.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Asset {
    /**
     *     Integer id; 资产id
     *     String name; 资产姓名
     *     String serialNumber; 资产编号
     *     String type; 资产类型
     *     String category; 资产分类
     *     Double price; 资产单价
     *     Double value; 资产净值
     *     String manufacturers; 资产厂商
     *     String supplier; 资产供应商
     */
    @Excel(name = "序号")
    Integer id;
    @Excel(name = "名称")
    String name;
    @Excel(name="编号")
    String serialNumber;
    @Excel(name = "型号")
    String type;
    @Excel(name = "类别")
    String category;
    @Excel(name = "单价")
    Double price;
    @Excel(name = "净值")
    Double value;
    @Excel(name = "厂家")
    String manufacturers;
    @Excel(name = "供应商")
    String supplier;
}
