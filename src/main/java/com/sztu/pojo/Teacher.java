package com.sztu.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class Teacher implements Serializable {
    @Excel(name = "教师id")
    Long id;
    @Excel(name = "教师名字")
    String name;
    @Excel(name = "教师账号")
    String account;
    @Excel(name = "教师密码")
    String password;
    @Excel(name = "权限")
    String authority;
    @ExcelEntity
    College college;
}
