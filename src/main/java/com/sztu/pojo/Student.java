package com.sztu.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Student {
    @Excel(name = "学生id")
    Long id;
    @Excel(name = "姓名")
    String name;
    @Excel(name = "账号")
    String account;
    @Excel(name = "密码")
    String password;
    @Excel(name = "权限")
    String authority;
    @ExcelEntity
    College college;
}
