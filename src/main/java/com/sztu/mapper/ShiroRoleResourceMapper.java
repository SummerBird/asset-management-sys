package com.sztu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sztu.pojo.ShiroRoleResource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-11-25
 */
@Mapper
public interface ShiroRoleResourceMapper extends BaseMapper<ShiroRoleResource> {
    public abstract List<String> getListByRoleLabel(String label);
}
