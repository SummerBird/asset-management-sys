package com.sztu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sztu.pojo.Device;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-12-02
 */
@Mapper
public interface DeviceMapper extends BaseMapper<Device> {
    public int insertByMap(Map<String,Object> map);
    public List<Map<String,Object>> getDeviceInfo();
}
