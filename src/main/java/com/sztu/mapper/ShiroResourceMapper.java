package com.sztu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sztu.pojo.ShiroResource;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface ShiroResourceMapper extends BaseMapper<ShiroResource> {
    public abstract Long insertByMap(Map map);
}
