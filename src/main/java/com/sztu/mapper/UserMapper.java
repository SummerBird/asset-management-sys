package com.sztu.mapper;

import com.sztu.pojo.User;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    public abstract List<User> getUsers();
    public abstract User queryUserByNameAndPwd(String name,String pwd);
    public abstract User queryUserByName(String name);
}
