package com.sztu.mapper;

import com.sztu.pojo.College;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface CollegeMapper {
    public abstract List<College> getAllCollege();
    public abstract College getCollegeById(Long id);
    public abstract College getCollegeByName(String collegeName);
    public abstract int addCollege(College college);
    public abstract int updateCollege(College college);
    public abstract int delCollegeById(Long id);
}
