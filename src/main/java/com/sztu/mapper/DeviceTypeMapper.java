package com.sztu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sztu.pojo.DeviceType;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-12-02
 */
@Mapper
public interface DeviceTypeMapper extends BaseMapper<DeviceType> {
    public int insertByMap(Map<String,Object> map);
}
