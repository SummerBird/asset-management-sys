package com.sztu.mapper;

import com.sztu.pojo.Teacher;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;


@Mapper
public interface TeacherMapper {
    public abstract List<Teacher> getAllTeacher();
    /***
     * 参数字段说明
     * name
     * account
     * authority
     * password
     * cid
     * collegeName
     */
    public abstract List<Teacher> queryTeacher(Map map);
    /***
     * 参数字段说明
     * name
     * account
     * authority
     * password
     * cid
     */
    public abstract int addTeacher(Map map);
    public abstract int delTeacherById(Integer id);
    /***
     * 参数字段说明
     * name
     * account
     * authority
     * password
     * cid
     * id
     */
    public abstract int updateTeacher(Map map);
}
