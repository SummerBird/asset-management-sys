package com.sztu.mapper;

import com.sztu.pojo.Asset;
import com.sztu.pojo.AssetInStorage;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AssetInStorageMapper {
    public abstract List<AssetInStorage> getAllAssetInStorage();
    public abstract List<Asset> getAllAssetNotInStorage();
    public abstract int addAssetInStorage(Map map);
    public abstract int delAssetInStorageById(Integer id);
}
