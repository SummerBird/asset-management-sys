package com.sztu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sztu.pojo.ShiroUserRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-11-25
 */
@Mapper
public interface ShiroUserRoleMapper extends BaseMapper<ShiroUserRole> {
    public abstract List<String> getRolesByUserId(Long id);
    public List<Map<String,Object>> selectAllByMaps();
}
