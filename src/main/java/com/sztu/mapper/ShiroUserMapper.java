package com.sztu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sztu.pojo.ShiroUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ShiroUserMapper extends BaseMapper<ShiroUser> {
    public List<Map<String,Object>> selectUserByRoleName(String roleName);
    public int insertSimpleShiroUser(Map<String,Object> map);
}
