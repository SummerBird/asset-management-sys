package com.sztu.mapper;

import com.sztu.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

@Mapper
public interface StudentMapper {
    public List<Student> getAllStudent();
    public List<Student> queryStudent(Map map);
    public int addStudent(Map map);
    public int delStudentById(Integer id);
    public int updateStudent(Map map);
}
