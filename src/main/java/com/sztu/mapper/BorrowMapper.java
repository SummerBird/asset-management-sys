package com.sztu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sztu.pojo.Borrow;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-12-13
 */
@Mapper
public interface BorrowMapper extends BaseMapper<Borrow> {
    public List<Map<String,Object>> getApproveInfoByUserId(Long userId);
    public List<Map<String,Object>> getApprovalsByUserId(Long userId);
}
