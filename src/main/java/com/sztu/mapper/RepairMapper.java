package com.sztu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sztu.pojo.Repair;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-12-21
 */
@Mapper
public interface RepairMapper extends BaseMapper<Repair> {
    public List<Map<String,Object>> getRepairInfoByReportUserId(Long reportUserId);
    public List<Map<String,Object>> getRepairInfoByRepairUserId(Long repairUserId);
}
