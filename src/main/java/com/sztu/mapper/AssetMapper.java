package com.sztu.mapper;

import com.sztu.pojo.Asset;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AssetMapper {
    /**
     * map 格式如下
     *         HashMap<String,Object> map=new HashMap<String,Object>();
     *         map.put("id",10); //updateAsset中必定要声明
     *         map.put("name","会议桌");
     *         map.put("serialNumber","S2104773");
     *         map.put("type","HUAWEI MateBook X Pro 2020款");
     *         map.put("category","仪器设备");
     *         map.put("price",10500.0);
     *         map.put("value",10500.0);
     *         map.put("manufacturers","华为");
     *         map.put("supplier","珠海乐活公社网络科技有限公司");
     *
     */
    public abstract List<Asset> getAllAsset();
    public abstract List<Asset> queryByLike(Map map);
    public abstract int addAsset(Map map);
    public abstract int updateAsset(Map map);
    public abstract int delAssetById(int id);
}
