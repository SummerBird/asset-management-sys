package com.sztu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sztu.pojo.ShiroRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShiroRoleMapper extends BaseMapper<ShiroRole> {

}
