#如何启动
启动类是`com.sztu.AssetManagementApplication`,运行这个类就可以启动了
#提交注意事项 
* 分支要为dev
* 提前前先跑下`com.sztu.utils.CleanLogsUtil`清下日志
* ```bash
  git add . //添加所有变动的文件
  git commit -m "xxx" // 提交 xxx为更新内容
  git push // 如果没有绑定远程分支则需要加上orgin/dev

  //关联远程分支
  git branch --set-upstream-to=origin/dev dev

